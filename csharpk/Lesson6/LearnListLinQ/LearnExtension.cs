﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnListLinQ
{
	public class LearnExtension
	{
		public void Print(string message)
		{
			Console.WriteLine($"C1- the value is {message}");
		}
	}


	public static class LearnExtension1
	{
		public static void Print(this string message)
		{
			Console.WriteLine($"C2- the value is {message}");
		}

		public static List<int> CreateList(this List<int> numbers, Func<int, bool> checkCondition)
		{
			var result = new List<int>();
			foreach (int number in numbers)
			{
				if (checkCondition(number))
				{
					result.Add(number);
				}
			}
			return result;
		}

	}
}
