﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnListLinQ
{
	public class LearnFunc
	{
		// nhập điểm của học sinh, điểm từ 0=>10
		public void InputScore()
		{
			var math = ValidateScoreWithCondition("math", MathCondition);
			var history = ValidateScore("history"); // 1-100
			var english = ValidateScore("english"); // 1-1000
			Console.WriteLine(math);
		}

		private double ValidateScore(string subject)
		{
			Console.WriteLine($"Input the {subject} score");
			var check = double.TryParse(Console.ReadLine(), out var score) && score >= 0 && score <= 10;
			while (!check)
			{
				Console.WriteLine($"The {subject} score is not correct,reinput");
				check = double.TryParse(Console.ReadLine(), out score) && score >= 0 && score <= 10;
			}
			return score;
		}

		public double ValidateScoreWithCondition(string subject, Func<double, bool> condition)
		{
			Console.WriteLine($"Input the {subject} score");
			var check = double.TryParse(Console.ReadLine(), out var score) && condition(score);
			while (!check)
			{
				Console.WriteLine($"The {subject} score is not correct,reinput");
				check = double.TryParse(Console.ReadLine(), out score) && condition(score);
			}
			return score;
		}

		public bool MathCondition(double score)
		{
			return score >= 0 && score <= 10;
		}

		public bool EnglishCondition(double score)
		{
			return score >= 0 && score <= 1000;
		}

		public List<int> CreateList(List<int> numbers, Func<int, bool> checkCondition)
		{
			var result = new List<int>();
			foreach (int number in numbers)
			{
				if (checkCondition(number))
				{
					result.Add(number);
				}
			}
			return result;
		}

	}
}
