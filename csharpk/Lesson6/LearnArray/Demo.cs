﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace LearnArray
{
	public class Demo
	{
		public void InputArray()
		{
			Console.WriteLine("input the array length");
			var length = int.Parse(Console.ReadLine());
			int[] numbers = new int[length];
			int sum = 0;
			for (int i = 0; i <= length - 1; i++)
			{
				Console.WriteLine($"input the element {i} of array");
				numbers[i] = int.Parse(Console.ReadLine());
				sum += numbers[i];
			}

			Console.WriteLine($"the avg is {(double)sum / length}");
		}
		// cho 1 mảng các số nguyên =>in ra các số lẻ trong mảng
		public void PrintOddNumbers()
		{
			int[] numbers = { 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
			foreach (int number in numbers)
			{
				if (number % 2 != 0)
				{
					Console.WriteLine(number);
				}
			}
		}
	}
}
