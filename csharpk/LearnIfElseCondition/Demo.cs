﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnIfElseCondition
{
    public class Demo
    {
        public string GetLearningResult(double score)
        {
            if (score < 5)
            {
                return "weak";
            }
            else if (score == 5)
            {
                return "avg";
            }
            else
            {
                return "quite good";
            }
        }

        public void RunGetLearningResult()
        {
            Console.WriteLine("Input the student score");
            try
            {
                double score = double.Parse(Console.ReadLine());
                var result = GetLearningResult(score);
                Console.WriteLine($"The learning result: {result}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("something went wrong");
            }

        }

        public void RunGetLearningResult1()
        {
            Console.WriteLine("Input the student score");
            if (double.TryParse(Console.ReadLine(), out double score) && score >= 0 && score <= 10)
            {
                var result = GetLearningResult(score);
                Console.WriteLine($"The learning result: {result}");
            }
            return;
        }

        public void CheckNumInteger()
        {
            Console.Write("Nhap mot so nguyen");
            string input = Console.ReadLine();

            if (int.TryParse(input, out int number))
            {
                if (number % 2 == 0)
                {
                    Console.WriteLine($"{number} la so chan");
                }
                else
                {
                    Console.WriteLine($"{number} là số lẻ.");
                }
            }
            else
            {
                Console.WriteLine("Đầu vào không hợp lệ. Vui lòng nhập một số nguyên.");
            }
        }

        public double FindMax(double a, double b, double c)
        {
            if (a >= b && a >= c)
            {
                return a;
            }
            else if (b >= a && b >= c)
            {
                return b;
            }
            else
            {
                return c;
            }
        }

        public void RunFindMax()
        {
            try
            {
                Console.WriteLine("Input the first number:");
                double num1 = double.Parse(Console.ReadLine());

                Console.WriteLine("Input the second number:");
                double num2 = double.Parse(Console.ReadLine());

                Console.WriteLine("Input the third number:");
                double num3 = double.Parse(Console.ReadLine());

                var maxNumber = FindMax(num1, num2, num3);
                Console.WriteLine($"The largest number is: {maxNumber}");
            }
            catch (Exception)
            {
                Console.WriteLine("One or more inputs are not correct numbers.");
                return;
            }
        }

        public bool CheckLeapYear(int year)
        {
            if (year % 100 == 0)
            {
                if (year % 400 == 0)
                {
                    return true;
                }
                return false;
            }
            else
            {
                if (year % 4 == 0)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CheckLeapYear1(int year)
        {
            if ((year % 100 == 0 && year % 400 == 0) || (year % 100 != 0 && year % 4 == 0))
            {
                return true;
            }
            return false;
        }

        public bool CheckLeapYear2(int year) => (year % 100 == 0 && year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);

        public void RunCheckLeapYear()
        {
            Console.WriteLine("Input the year");
            if (int.TryParse(Console.ReadLine(), out int year) && year >0)
            {
                string result = CheckLeapYear(year) ? $"{year} is leap year" : $"{year} is not leap year";
                Console.WriteLine(result);
            }
        }
    }
}
