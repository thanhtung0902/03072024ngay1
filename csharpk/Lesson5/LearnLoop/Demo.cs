﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnLoop
{
	public class Demo
	{
		public void PrintOddNumners(int limit)
		{
			for (int i = 0; i < limit; i++)
			{
				if (i % 2 != 0)
				{
					Console.WriteLine(i);
				}
			}

		}

		public void InputStudents()
		{
			try
			{
				Console.WriteLine("Enter the number of students");
				var numbers = int.Parse(Console.ReadLine());

				for (int i = 1; i <= numbers; i++)
				{
					Console.WriteLine($"input the name of student {i}");
					var name = Console.ReadLine();

					Console.WriteLine($"input the age of student {i}");
					var age = int.Parse(Console.ReadLine());

					Console.WriteLine($"input the address of student {i}");
					var address = Console.ReadLine();

					Console.WriteLine($"name of student {i}: {name}, age of student {i}: {age}, address of student {i}: {address}.");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
