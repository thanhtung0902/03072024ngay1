﻿using LearnOOP;
using LearnOOP.Products;

var anna = new Student();
anna.Id = 1;
var id = anna.Id;
anna.Age = 14;
anna.Name = "anna";
var name = anna.Name;

var bob = new Student2();
bob.Id = 2;
bob.Name = "Bob";
bob.Age = 14;
bob.Address = "cg";


var ronaldo = new Student2()
{
	Id = 3,
	Name = "ronaldo",
	Age = 15,
	Address = "xp"
};


Console.WriteLine(bob.Info);

var manager = new ProductManager();
manager.Init();

Console.ReadKey();
